'use strict';
module.exports = function(grunt) {
    grunt.initConfig({
        assetsPath: 'assets',
        watch: {
            sass: {
                files: ['<%= assetsPath %>/css/scss/**/*.{scss,sass}'],
                tasks: ['sass']
            },
            js: {
                files: ['<%= assetsPath %>/js/**/*.js'],
                tasks: ['uglify']
            },
            images: {
                files: ['<%= assetsPath %>/images/**/*.{png,jpg,gif}'],
                tasks: ['imagemin']
            },
        },

        // sass
        sass: {
            dist: {
                options: {
                    style: 'compressed'
                },
                files: {
                    '<%= assetsPath %>/css/styles.min.css': '<%= assetsPath %>/css/scss/styles.scss'
                }
            }
        },

        // uglify to concat, minify, and make source maps
        uglify: {
            main: {
                options: {
                    sourceMap: '<%= assetsPath %>/js/main.js.map',
                    sourceMappingURL: 'main.js.map',
                    sourceMapPrefix: 2
                },
                files: {
                    '<%= assetsPath %>/js/main.min.js': [
                        '<%= assetsPath %>/js/source/jquery-1.11.1.js',
                        '<%= assetsPath %>/js/source/main.js'
                    ]
                }
            }
        },

        // image optimization
        imagemin: {
            dist: {
                options: {
                    optimizationLevel: 7,
                    progressive: true,
                    interlaced: true
                },
                files: [{
                    expand: true,
                    cwd: '<%= assetsPath %>/images/',
                    src: ['**/*.{png,jpg,gif}'],
                    dest:'<%= assetsPath %>/images/'
                }]
            }
        }
    });

    // load all grunt tasks matching the `grunt-*` pattern
    require('load-grunt-tasks')(grunt);
    // register task
    grunt.registerTask('default', ['sass', 'uglify', 'watch']);

};