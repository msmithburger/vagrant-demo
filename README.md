# Vagrant Demo README #

In order to get things up and running you need to...

1. Install [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
2. Install [Vagrant](https://www.vagrantup.com/downloads.html) 
3. Clone this repository
4. From the command line, navigate into the root of the cloned repository and run: 
```
#!bash

vagrant up
```


### It's up... now what? ###

Your project is now available at: http://127.0.0.1:1234
You can edit the files using your favorite editor without entering the virtual server. If you want to access the server itself (say, to run grunt or npm) you gain access with:
```
#!bash

vagrant ssh
```

Once in, you can access your local files in the VM here: 
```
#!bash

cd /vagrant
```

You can also install anything you would install in a normal Linux server since what you are running is a bog standard Ubuntu instance (apt-get all the things!)


### Questions? ###

Contact either Matt Smithburger or Stuart Tannehill