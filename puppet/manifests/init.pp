class { 'stdlib': }
class { 'wget': }
class { 'apt': }
class { 'nodejs': }
package {'grunt-cli':
    ensure   => present,
    provider => 'npm',
    require  => Class['nodejs'],
}
package { ['sass', 'compass']:
  ensure => 'installed',
  provider => 'gem',
}
class { 'apache2::install': }
